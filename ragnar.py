from PPlay.sprite import *

class Ragnar():
  gravidade = 100

  def __init__(self, speed, window):
    self.estado = "parado"
    self.lifes = 5
    self.pulando = False
    self.altura_base = 150
    self.altura_do_pulo = window.height - self.altura_base - 100
    self.cronometro_pulo = 2
    self.x_axis_direction = 0
    self.sprites = {
      "parado": Sprite("ragnar_parado.png", 4),
      "andando_direita": Sprite("ragnar_andando_direita.png", 6),
      "andando_esquerda": Sprite("ragnar_andando_esquerda.png", 6),
      "pulando_direita": Sprite("ragnar_pulando_direita.png", 5),
      "pulando_esquerda": Sprite("ragnar_pulando_esquerda.png", 5),
      "atacando_direita": Sprite("ragnar_atacando_esquerda.png", 3),
      "atacando_esquerda": Sprite("ragnar_atacando_direita.png", 3)
    } 

    for sprite in self.sprites:
      sprite_da_vez = self.sprites[sprite]
      sprite_da_vez.set_position(window.width/10, window.height - sprite_da_vez.height - 65)
      if sprite == "atacando_direita" or sprite == "atacando_esquerda":
        sprite_da_vez.set_total_duration(300)
      else:
        sprite_da_vez.set_total_duration(2000)
    
    self.y_axis_position = sprite_da_vez.y
    self.speed_per_second = speed

 
  def gravity_handler(self,window):
    altura_do_chao = window.height - self.altura_base
    ta_no_chao = (self.y_axis_position >= altura_do_chao)
    if not self.pulando and not ta_no_chao:
      self.move_vertically(window, 1.5)
    if ta_no_chao:
      self.cronometro_pulo = 2

  
  def jump_handler(self, window):
    na_altura_maxima = (self.y_axis_position <= self.altura_do_pulo)
    if (self.pulando and not na_altura_maxima):
      self.move_vertically(window, -2)
    if na_altura_maxima:
      self.pulando = False

    self.cronometro_pulo -= window.delta_time()
    self.gravity_handler(window)

    
  def standing(self, window):
    self.estado = "parado"
    self.zera_animacao(window)
    
  def walk_to_right(self, window):
    self.estado = "andando_direita"
    if self.get_x() < (window.width - self.sprites["andando_direita"].width):
      self.move_horizontally("right", window)
    
  def walk_to_left(self, window):
    self.estado = "andando_esquerda"
    if self.get_x() > 0:
      self.move_horizontally("left", window)

  def jump_standing(self, window):
    self.estado = "parado"
    if (self.cronometro_pulo >= 2):
      self.pulando = True

  def jump_to_right(self, window):
    self.x_axis_direction = 1
    self.estado = "pulando_direita"
    if (self.cronometro_pulo >= 2):
      self.pulando = True
    self.move_horizontally("right",window)

  def jump_to_left(self, window):
    self.x_axis_direction = -1
    self.estado = "pulando_esquerda"
    if (self.cronometro_pulo >= 2):
      self.pulando = True
    self.move_horizontally("left",window)

  def attack_to_right(self, window):
    self.estado = "atacando_direita"

  def attack_to_left(self, window):
    self.estado = "atacando_esquerda"

  def move_horizontally(self, direction, window):
    if direction == "right":
      self.x_axis_direction = 1
    elif direction == "left":
      self.x_axis_direction = -1
    # move todo mundo pra mesma direção
    for sprite in self.sprites:
      sprite_da_vez = self.sprites[sprite]
      sprite_da_vez.move_x(self.speed_per_second * window.delta_time() * self.x_axis_direction * 2)

  def move_vertically(self, window, value):
    for sprite in self.sprites:
      sprite_da_vez = self.sprites[sprite]
      sprite_da_vez.move_y(self.speed_per_second * window.delta_time() * value)
    self.y_axis_position = sprite_da_vez.y

  def zera_animacao(self, window):
    for sprite in self.sprites:
      if sprite != "parado":
        self.sprites[sprite].set_curr_frame(0)

  def get_x(self):
    return self.sprites[self.estado].x

  def set_x(self, window):
    for sprite in self.sprites:
      self.sprites[sprite].x = window.width/10

  def get_width(self):
    return self.sprites[self.estado].width

  def movement(self,window):
    keyboard = window.get_keyboard()
    user_pressed_left_key = keyboard.key_pressed("left")
    user_pressed_right_key = keyboard.key_pressed("right")
    user_pressed_space_key = keyboard.key_pressed("space")
    user_pressed_Z_key = keyboard.key_pressed("Z")
    
    if not self.pulando and (user_pressed_space_key and user_pressed_right_key) :
      self.jump_to_right(window)
    elif not self.pulando and (user_pressed_space_key and user_pressed_left_key):
      self.jump_to_left(window)
    elif not self.pulando and (user_pressed_space_key):
      self.jump_standing(window)
    elif user_pressed_left_key:
      self.walk_to_left(window)
    elif user_pressed_right_key:
      self.walk_to_right(window)
    elif user_pressed_Z_key:
      if self.x_axis_direction == -1:
        self.attack_to_right(window)
      elif self.x_axis_direction == 1:
        self.attack_to_left(window)
    else:
      self.standing(window)
    
    self.jump_handler(window)

  def draw(self, window):
    self.sprites[self.estado].draw()
    self.sprites[self.estado].update()

  def take_damage(self):
    self.lifes -= 1
    print('vidas: {}'.format(self.lifes))
    