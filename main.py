from ragnar import *
from esqueleto import *
from PPlay.window import *
from PPlay.gameimage import *
import random
from PPlay.collision import *



janela = Window(929,593)
janela.set_title("For Valhalla")
teclado = janela.get_keyboard()
fundo = GameImage("fundo.png")
menu_fundo = GameImage("menu.png")
chao = Sprite("chão.png")
pedra = Sprite("pedra.png")
ragnar = Ragnar(200, janela)
pedra.set_position(janela.width*4/5, janela.height - pedra.height - 50)
chao.set_position(0, janela.height - chao.height)
fundo_musical = pygame.mixer.music.load('fundo_musical.mp3')
pygame.mixer.music.play()
inimigos = []

pontos = 0

streak = 1

ultimo_esqueleto = 6

ultimo_dano_tomado = 1.5

ultimo_dano_dado = 0
gamestate = 0
tempo_de_jogo = 0
desenho_vidas = [Sprite('./vida.png'), Sprite('./vida.png'), Sprite('./vida.png'), Sprite('./vida.png'), Sprite('./vida.png')]

def esqueletoSpawnCD():
    if tempo_de_jogo < 20:
        return 15
    if tempo_de_jogo >= 20 and tempo_de_jogo < 40:
        return 10
    if tempo_de_jogo >= 40 and tempo_de_jogo < 60:
        return 8
    if tempo_de_jogo >= 60:
        return 5
    
def game_loop():
    global pontos
    global ultimo_esqueleto
    global ultimo_dano_tomado
    global ultimo_dano_dado
    global streak
    global tempo_de_jogo
    global gamestate

    janela.set_background_color((0,0,0))
    fundo.draw()
    janela.draw_text("For Valhalla", janela.width/3 +20, 30, size=40, color=(255, 255,255), font_name="Arial", bold=False, italic=False)
    janela.draw_text(f"{pontos}", janela.width - 100, 30, size=40, color=(255, 255,255), font_name="Arial", bold=False, italic=False)
    pedra.draw()
    chao.draw()
    ragnar.movement(janela)
    ragnar.draw(janela)

    if ultimo_esqueleto >= esqueletoSpawnCD():
        novoEsqueleto = Esqueleto(janela, random.choice([-1, 1]))
        inimigos.append(novoEsqueleto)
        ultimo_esqueleto = 0

    # checa dano no ragnar
    for esqueleto in inimigos:
        esqueleto.follow(ragnar)
        esqueleto.walk(ragnar)
        if esqueleto.checa_dano(ragnar):
            if ultimo_dano_tomado >= 1.5:
                ragnar.take_damage() 
                ultimo_dano_tomado = 0  
                streak = 1
        esqueleto.draw()
    
    #vê se o esqueleto morreu e se sim some com ele
    for esqueleto in inimigos:
        distancia_suficiente = esqueleto.get_distance(ragnar) < 5
        if ragnar.get_x() > esqueleto.x:
            if distancia_suficiente and (ragnar.estado == "atacando_direita"):
                if ultimo_dano_dado >= 0.5:
                    if esqueleto.lifes >= 1:
                        esqueleto.take_damage()
                    else:
                        inimigos.remove(esqueleto)
                        pontos += streak
                        streak += 1
                    ultimo_dano_dado = 0
        elif ragnar.get_x() < esqueleto.x:
            if distancia_suficiente and (ragnar.estado == "atacando_esquerda"):
                if ultimo_dano_dado >= 0.5:
                    if esqueleto.lifes >= 1:
                        esqueleto.take_damage()
                    else:
                        inimigos.remove(esqueleto)
                        pontos += streak
                        streak += 1
                    ultimo_dano_dado = 0
    if ragnar.lifes <= 0:
        gamestate = 2
    draw_vidas(ragnar)

    ultimo_esqueleto += janela.delta_time()
    ultimo_dano_tomado += janela.delta_time()
    ultimo_dano_dado += janela.delta_time()
    tempo_de_jogo += janela.delta_time()
    janela.update()

def menu():
    global gamestate
    global pontos
    menu_fundo.draw()
    janela.draw_text("For Valhalla", janela.width/3 , 30, size=60, color=(255, 255,255), font_name="Arial", bold=False, italic=False)
    janela.draw_text("Pressione ENTER para começar", janela.width/3 +10 , janela.height *2/3 +10, size=20, color=(255, 255,255), font_name="Arial", bold=False, italic=False)
    janela.update()
    if teclado.key_pressed("enter"):
        gamestate = 1
        pontos = 0
    
def game_over():
    global gamestate
    global inimigos
    global pontos
    global tempo_de_jogo
    janela.set_background_color((0,0,0))
    janela.draw_text(f"{pontos}", janela.width - 100, 30, size=40, color=(255, 255,255), font_name="Arial", bold=False, italic=False)
    janela.draw_text("Game Over", janela.width/4 +50, janela.height/3 + 20, size=72, color=(255,0,0), font_name="Arial", bold=False, italic=False)
    janela.update()
    time.sleep(2)
    ragnar.lifes = 5
    inimigos = []
    pontos = 0
    tempo_de_jogo = 0
    ragnar.set_x(janela)
    gamestate = 0


def draw_vidas(personagem):
    for i in range(personagem.lifes):
        vida = desenho_vidas[i]
        vida.set_position(i * vida.width + 2, 5)
        vida.draw()

while True:
    if gamestate == 0:
        menu()
    elif gamestate == 1:
        game_loop()
    elif gamestate == 2:
        game_over()
