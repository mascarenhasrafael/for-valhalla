from PPlay.sprite import *

class Esqueleto():

    def __init__(self, window, dir_inicial):
        self.estado = "andando_esquerda"
        self.lifes = 2
        self.altura_base = 150
        self.x_axis_direction = dir_inicial
        self.sprites = {
            "andando_direita": Sprite('./esqueleto/esqueleto_andando_direita.png', 13),
            "andando_esquerda": Sprite('./esqueleto/esqueleto_andando_esquerda.png', 13),
            "atacando_direita": Sprite('./esqueleto/esqueleto_atacando_direita.png', 18),
            "atacando_esquerda": Sprite('./esqueleto/esqueleto_atacando_esquerda.png', 18)
        }

        for sprite in self.sprites:
            sprite_da_vez = self.sprites[sprite]
            if dir_inicial == -1:
                sprite_da_vez.set_position(window.width , window.height - self.altura_base)
            elif dir_inicial == 1:
                sprite_da_vez.set_position(-22 , window.height - self.altura_base)
            sprite_da_vez.set_total_duration(2000)

        self.x = sprite_da_vez.x

    def zera_animacao(self):
        for sprite in self.sprites:
            if sprite != self.estado:
                self.sprites[sprite].set_curr_frame(0)

    def get_distance(self, target):
        if self.x < target.get_x():
            return target.get_x() - self.x - self.sprites[self.estado].width
        elif self.x > target.get_x():
            return self.x - target.get_x() - target.get_width()

    def walk(self, target):
        if self.get_distance(target) < 3:
            if self.x_axis_direction == 1:
                self.estado = "atacando_direita"
            elif self.x_axis_direction == -1:
                self.estado = "atacando_esquerda"
        else:
            if self.x_axis_direction == -1:
                self.estado = "andando_esquerda"
                for sprite in self.sprites:
                    self.sprites[sprite].move_x(-1/5)
                    self.zera_animacao()
                self.x = self.sprites["andando_esquerda"].x
            if self.x_axis_direction == 1:
                self.estado = "andando_direita"
                for sprite in self.sprites:
                    self.sprites[sprite].move_x(1/5)
                    self.zera_animacao()
                self.x = self.sprites["andando_direita"].x

    def change_direction(self):
        if self.x_axis_direction == 1:
            self.x_axis_direction = -1
        elif self.x_axis_direction == -1:
            self.x_axis_direction = 1

    def follow(self, target):
        if self.x_axis_direction == -1 and target.get_x() > self.x:
            self.change_direction()
        elif self.x_axis_direction == 1 and target.get_x() < self.x:
            self.change_direction()
    
    def checa_dano(self, target):
        if self.estado == "atacando_direita" or self.estado == "atacando_esquerda":
            if self.sprites[self.estado].get_curr_frame() == 8:
                if self.get_distance(target) <= 10:
                    return True
        return False

    def take_damage(self):
        self.lifes -= 1


    def draw(self):
        self.sprites[self.estado].draw()
        self.sprites[self.estado].update()
